## Helm Charts [![pipeline status](https://gitlab.cern.ch/helm/charts/badges/master/pipeline.svg)](https://gitlab.cern.ch/helm/charts/commits/master)

This is the CERN Helm Chart repository.

For more information about installing and using Helm, see its [README.md](https://github.com/kubernetes/helm/tree/master/README.md).

To get a quick introduction to Charts see this [chart document](https://github.com/kubernetes/helm/blob/master/docs/charts.md).

## How do i enable this repository?

To add the CERN Charts repository, run `helm repo add`:
```
helm repo add cern https://s3-website.cern.ch/cern-charts
```

To check available charts do a `helm search` or check the [index file](http://s3.cern.ch/cern-charts/index.yaml).

## How do i install these charts?

After enabling the repository, just:
```
helm install <chart>
```
For more information on using Helm, refer to the [Helm's documentation](https://github.com/kubernetes/helm#docs).

## Contributing a Chart

If you would like to add a chart to this repository, please open a Merge
Request with the new chart.

